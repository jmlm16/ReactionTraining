#include <LiquidCrystal.h>

#define trig 5
#define echo 4
#define led 3
#define buzz 6
#define sw_pin A0
#define joy A5


void menu(char*, int*, char*);
int randomMiliseconds();

LiquidCrystal lcd(8, 7, A3, A2, A1, A4);

int serie = 3, index = 0, descanso_accion = 4, tiempo_serie = 120,descanso_serie = 30;
bool buzzer = false,leds = false;
long distancia;
long tiempo;

void setup(){
  Serial.begin(9600);
  pinMode(led,OUTPUT);
  pinMode(sw_pin,INPUT);
  pinMode(trig, OUTPUT);
  pinMode(echo, INPUT);
  digitalWrite(sw_pin, HIGH);
  lcd.begin(16, 2);
  lcd.print("ReactionTraining");
  delay(2000);
  lcd.clear();
  config();
}

void loop(){
  while(index < serie){
    lcd.clear();
    lcd.print("serie ");lcd.print(index +1);lcd.print(" de ");lcd.print(serie);
    int time_loop = (millis() / 1000) + tiempo_serie;
    
    while(time_loop > millis()/1000){
      
      digitalWrite(trig,LOW);
      delayMicroseconds(5);
      digitalWrite(trig, HIGH);
      delayMicroseconds(10);
      digitalWrite(led,HIGH);
      analogWrite(buzz,70);
      tiempo=pulseIn(echo, HIGH);
      distancia= int((tiempo * 0.017));
      Serial.println(distancia);
      if(distancia < 20 ){
        Serial.println(distancia);
        digitalWrite(led,LOW);
        analogWrite(buzz,0);
        delay(randomMiliseconds() + 1000);
      }
    }
  index++;
  analogWrite(buzz,0);
  digitalWrite(led,LOW);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Descanso");
  delay(descanso_serie * 1000);
  }
  lcd.clear();
  lcd.print("FIN");
  delay(10000);
  
}

int randomMiliseconds(){
  return rand() % descanso_accion  * 1000;
}
void menu(char* title, int* var, char* ext){
  lcd.setCursor(0, 0);
  lcd.print(title);
  lcd.setCursor(0, 1);
  lcd.print(*var);
  while(digitalRead(sw_pin) == 1){
    
    if(analogRead(joy) > 700){
      *var = *var + 1;
      lcd.setCursor(0, 1);
      lcd.print(*var);
      delay(300); 
    }else if (analogRead(joy) < 300){
      *var = *var - 1;
      lcd.setCursor(0, 1);
      lcd.print(*var);
      delay(300);
    }
  }
  lcd.clear();
  lcd.setCursor(0, 1);
  lcd.print(title);
  lcd.print("= ");
  lcd.print(*var);
  lcd.print(ext);
  delay(1000);
}

void config() {

  menu("n de series", &serie, " repeticiones");
  menu("t de series", &tiempo_serie, " seg");
  menu("d series", &descanso_serie, " seg");
  menu("t acciones", &descanso_accion, " seg");
  
}
