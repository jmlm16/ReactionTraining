//#include <Arduino.h>
//#include <stdio.h>
#include <LiquidCrystal.h>

#define trig 12
#define echo 11
#define led 10
#define buzz 3
#define bot 9
#define sw_pin 13

void menu(char*, int*, char*);
int randomMiliseconds();

LiquidCrystal lcd(4, 5, 6, 7, 8, 9);

int serie = 3, index = 0, descanso_accion = 4, tiempo_serie = 120,descanso_serie = 30;
bool buzzer = false,leds = false;
long distancia;
long tiempo;

void setup(){
  Serial.begin(9600);
  pinMode(led,OUTPUT);
  pinMode(sw_pin,INPUT);
  pinMode(trig, OUTPUT); /*activación del pin 9 como salida: para el pulso ultrasónico*/
  pinMode(echo, INPUT); /*activación del pin 8 como entrada: tiempo del rebote del ultrasonido*/
  leds = true;
  digitalWrite(sw_pin, HIGH);
  lcd.begin(16, 2);
  lcd.print("ReactionTraining");
  delay(2000);
  lcd.clear();
  config();
}

void loop(){
  while(index < serie){
    lcd.clear();
    lcd.print("serie ");lcd.print(index +1);lcd.print(" de ");lcd.print(serie);
    int time_loop = (millis() / 1000) + tiempo_serie;
    
    while(time_loop > millis()/1000){
      
      digitalWrite(trig,LOW); /* Por cuestión de estabilización del sensor*/
      delayMicroseconds(5);
      digitalWrite(trig, HIGH); /* envío del pulso ultrasónico*/
      delayMicroseconds(10);
      if(leds){digitalWrite(led,HIGH);}
      if(buzzer){analogWrite(buzz,70);}
      tiempo=pulseIn(echo, HIGH); /* Función para medir la longitud del pulso entrante. Mide el tiempo que transcurrido entre el envío
      del pulso ultrasónico y cuando el sensor recibe el rebote, es decir: desde que el pin echo empieza a recibir el rebote, HIGH, hasta que
      deja de hacerlo, LOW, la longitud del pulso entrante*/
      distancia= int((tiempo * 0.017)); /*fórmula para calcular la distancia obteniendo un valor entero*/
      
      if(distancia < 20 ){
        if(leds){digitalWrite(led,LOW);}
        if(buzzer){analogWrite(buzz,0);}
        delay(randomMiliseconds() + 1000);
      }
    }
  index++;
  digitalWrite(led,LOW);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Descanso");
  delay(descanso_serie * 1000);
  }
  lcd.clear();
  lcd.print("FIN");
  delay(10000);
  
}

int randomMiliseconds(){
  return rand() % descanso_accion  * 1000;
}
void menu(char* title, int* var, char* ext){
  lcd.setCursor(0, 0);
  lcd.print(title);
  lcd.setCursor(0, 1);
  lcd.print(*var);
  while(digitalRead(sw_pin) == 1){
    
    if(analogRead(2) > 700){
      *var = *var + 1;
      lcd.setCursor(0, 1);
      lcd.print(*var);
      delay(300); 
    }else if (analogRead(2) < 300){
      *var = *var - 1;
      lcd.setCursor(0, 1);
      lcd.print(*var);
      delay(300);
    }
  }
  lcd.clear();
  lcd.setCursor(0, 1);
  lcd.print(title);
  lcd.print("= ");
  lcd.print(*var);
  lcd.print(ext);
  delay(1000);
}

void config() {

  menu("numero de series", &serie, " repeticiones");
  menu("tiempo de series", &tiempo_serie, " seg");
  menu("descanso series", &descanso_serie, " seg");
  menu("margen acciones", &descanso_accion, " seg");
/* 
  lcd.setCursor(0, 0);
  lcd.print("numero de series");
  lcd.setCursor(0, 1);
  lcd.print(serie);
  while(digitalRead(sw_pin) == 1){
    
    if(analogRead(2) > 700){
      serie = serie + 1;
      lcd.setCursor(0, 1);
      lcd.print(serie);
      delay(300); 
    }else if (analogRead(2) < 300){
      serie = serie - 1;
      lcd.setCursor(0, 1);
      lcd.print(serie);
      delay(300);
    }
  }
  lcd.clear();
  lcd.setCursor(0, 1);
  lcd.print("serie = ");
  lcd.print(serie);
  lcd.print("rep");
  delay(1000);
  
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("tiempo de series");
  lcd.setCursor(0, 1);
  lcd.print(tiempo_serie);
  while(digitalRead(sw_pin) == 1){
    
    if(analogRead(2) > 700){
      tiempo_serie = tiempo_serie + 1;
      lcd.setCursor(0, 1);
      lcd.print(tiempo_serie);
      delay(300); 
    }else if (analogRead(2) < 300){
      tiempo_serie = tiempo_serie - 1;
      lcd.setCursor(0, 1);
      lcd.print(tiempo_serie);
      delay(300);
    }
  }
  lcd.clear();
  lcd.setCursor(0,1);
  lcd.print("t serie = ");
  lcd.print(tiempo_serie);
  lcd.print("seg");
  delay(1000);
  
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("descanso series");
  lcd.setCursor(0, 1);
  lcd.print(descanso_serie);
  while(digitalRead(sw_pin) == 1){
    
    if(analogRead(2) > 700){
      descanso_serie = descanso_serie + 1;
      lcd.setCursor(0, 1);
      lcd.print(descanso_serie);
      delay(300); 
    }else if (analogRead(2) < 300){
      descanso_serie = descanso_serie - 1;
      lcd.setCursor(0, 1);
      lcd.print(descanso_serie);
      delay(300);
    }
  }
  lcd.clear();
  lcd.setCursor(0, 1);
  lcd.print("Descanso = ");
  lcd.print(descanso_serie);
  lcd.print("seg");
  delay(1000);

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("descanso accion");
  lcd.setCursor(0, 1);
  lcd.print(descanso_accion);
  while(digitalRead(sw_pin) == 1){
    
    if(analogRead(2) > 700){
      descanso_accion = descanso_accion + 1;
      lcd.setCursor(0, 1);
      lcd.print(descanso_accion);
      delay(300); 
    }else if (analogRead(2) < 300){
      descanso_accion = descanso_accion - 1;
      lcd.setCursor(0, 1);
      lcd.print(descanso_accion);
      delay(300);
    }
  }
  lcd.clear();
  lcd.setCursor(0, 1);
  lcd.print("Descanso acc = ");
  lcd.print(descanso_accion);
  lcd.print("seg");
  delay(1000);
  lcd.clear();
*/
}
