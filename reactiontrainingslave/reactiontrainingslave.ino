
#define trig 5
#define echo 4
#define led 3
#define buzz 6

int randomMiliseconds();

int serie = 3, index = 0, descanso_accion = 4, tiempo_serie = 120,descanso_serie = 30;
long distancia;
long tiempo;

void setup(){
  Serial.begin(9600);
  pinMode(led,OUTPUT);
  pinMode(trig, OUTPUT); /*activación del pin 9 como salida: para el pulso ultrasónico*/
  pinMode(echo, INPUT); /*activación del pin 8 como entrada: tiempo del rebote del ultrasonido*/
  delay(2000);
}

void loop(){
  while(index < serie){
    int time_loop = (millis() / 1000) + tiempo_serie;
    
    while(time_loop > millis()/1000){
      
      digitalWrite(trig,LOW);
      delayMicroseconds(5);
      digitalWrite(trig, HIGH);
      delayMicroseconds(10);
      digitalWrite(led,HIGH);
      analogWrite(buzz,70);
      tiempo=pulseIn(echo, HIGH);
      distancia= int((tiempo * 0.017));
      
      if(distancia < 20 ){
        digitalWrite(led,LOW);
        analogWrite(buzz,0);
        delay(randomMiliseconds() + 1000);
      }
    }
  index++;
  delay(descanso_serie * 1000);
  }
}

int randomMiliseconds(){
  return rand() % descanso_accion  * 1000;
}
